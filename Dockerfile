FROM alpine:latest

LABEL maintainer="pekopekoda / gitlab.com"

RUN apk add --no-cache curl

CMD ["/usr/sbin/crond", "-f"]
